import React from "react";
import { useState, useEffect } from "react";
import Card from "../Card/Card";
import "./Products.css";
import { InfinitySpin } from "react-loader-spinner";

function Products() {
  const API_STATES = {
    LOADING: "loading",
    LOADED: "loaded",
    ERROR: "error",
  };

  const [productsDetail, setProductsDetail] = useState({
    productsData: [],
    status: API_STATES.LOADING,
  });

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => {
        return res.json();
      })
      .then((responseData) => {
        setProductsDetail((prev) => {
          return {
            ...prev,
            productsData: responseData,
            status: API_STATES.LOADED,
          };
        });
      })
      .catch((error) => {
        console.error(error.message);
        setProductsDetail((prev) => {
          return {
            ...prev,
            status: API_STATES.ERROR,
          };
        });
      });
  }, []);
  return productsDetail.status === API_STATES.LOADING ? (
    <div className="spinner">
      <InfinitySpin width="200" color="#4fa94d" />
    </div>
  ) : productsDetail.status === API_STATES.ERROR ? (
    <h1>Data not Fetched</h1>
  ) : productsDetail.length === 0 ? (
    <h1>Internal server error</h1>
  ) : (
    <div className="products">
      {productsDetail.productsData.map((product) => {
        return <Card product={product} key={product.id} />;
      })}
    </div>
  );
}

export default Products;
