import React from 'react'
import './Header.css'

function Header() {
  return (
    <div className="header">
      <span>
        <i className="fa-solid fa-house cart-icon"></i>
      </span>
      <span className="product-type">
        <span className="product-link">Home</span>
        <span className="product-link">Electronics</span>
        <span className="product-link">Grocerry</span>
      </span>
      <span className="cart-icon">
        <i className="fa-solid fa-cart-shopping"></i>
      </span>
    </div>
  )
}

export default Header
