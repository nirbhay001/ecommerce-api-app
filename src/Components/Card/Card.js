import React from 'react'
import './Card.css'

function Card({product}) {
  return (
    <div className="card" key={product.key}>
    <img src={product.image} alt="img" />
    <div>
      Price<span className="text-bold"> : {product.price}$</span>
    </div>
    <div>{product.title}</div>
    <div>
      Rating<span className="text-bold"> : {product.rating.rate}</span>
    </div>
    <div>
      Category<span className="text-bold"> : {product.category}</span>
    </div>
    <div>
      Count<span className="text-bold"> : {product.rating.count}</span>
    </div>
  </div>
  )
}

export default Card
